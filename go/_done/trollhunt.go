package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b, c float64
	fmt.Scan(&a, &b, &c)
	group := math.Floor(b / c)
	days := (a - 1.0) / group
	if (days - math.Floor(days)) > 0.0 {
		fmt.Println(int(days) + 1)
	} else {
		fmt.Println(int(days))
	}
}
