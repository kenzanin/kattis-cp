#include "bits/stdc++.h"

int main() {
  int in1;
  std::cin >> in1;
  for (int i = 1; i < in1 + 1; i++) {
    std::string in2{};
    std::cin >> in2;
    std::cout << ((i & 1) == 0 ? "" : in2 + "\n");
  }
}