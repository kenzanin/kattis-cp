#include "bits/stdc++.h"

int main() {
  auto jebb = std::unordered_map<std::string, std::string>{};
  int a;
  std::cin >> a;
  std::cin.ignore();
  for (int i = 0; i < a; i++) {
    std::string tmp{};
    std::getline(std::cin, tmp);
    if (tmp.find(" ") != tmp.npos) {
      char a[20]{}, b[20]{};
      std::sscanf(tmp.c_str(), "%s %s", a, b);
      jebb[a] = b;
    } else {
      jebb[tmp] = "";
    }
  }

  std::cin >> a;
  for (int i = 0; i < a; i++) {
    std::string tmp;
    std::cin >> tmp;
    if (jebb.find(tmp) == jebb.end()) {
      std::cout << "Neibb\n";
      continue;
    }
    if (jebb[tmp].length() == 0) {
      std::cout << "Jebb\n";
    } else {
      std::cout << "Neibb en " << tmp << " " << jebb[tmp] << " er heima\n";
    }
  }
}