package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

func main() {
	a := 0
	fmt.Scan(&a)

	input := bufio.NewScanner(os.Stdin)
	data := make([]int, a)
	for i := 0; i < a; i++ {
		input.Scan()
		b := input.Text()
		tmp := ""
		fmt.Sscanf(b, "%s %d", &tmp, &data[i])
	}
	sort.Slice(data, func(i, j int) bool { return data[i] > data[j] })
	if len(data)&1 == 1 {
		data = append(data, 0)
	}
	harga := 0
	for i := 0; i < len(data)-1; i += 2 {
		harga += data[i]
	}
	fmt.Println(harga)
}
