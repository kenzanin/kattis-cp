package main

import "fmt"

func main() {
	var a, b int
	c := 0
	fmt.Scan(&a, &b)
	if a < b {
		fmt.Println(b - a)
	} else if a == b {
		fmt.Println(0)
	} else {
		for a > b {
			if a&1 == 1 {
				a++
				c++
			}
			a >>= 1
			c++
		}
		fmt.Println(c + (b - a))
	}
}
