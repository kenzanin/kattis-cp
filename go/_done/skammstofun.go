package main

import (
	"fmt"
)

func main() {
	a := 0
	fmt.Scan(&a)
	b := ""
	for i := 0; i < a; i++ {
		fmt.Scan(&b)
		if b[0] < 'A' || b[0] > 'Z' {
			continue
		}
		fmt.Printf("%c", b[0])
	}
}
