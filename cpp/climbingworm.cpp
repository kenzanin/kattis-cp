#include "bits/stdc++.h"

int main() {
  int a, b, c, res{};
  std::cin >> a >> b >> c;
  while (c >= 0) {
    res++;
    c -= a;
    if (c <= 0)
      break;
    c += b;
  }
  std::cout << res << "\n";
}