package main

import (
	"fmt"
)

func main() {
	mins := func(a string) bool {
		for i := 0; i < len(a); i++ {
			if a[i] != '.' {
				return true
			}
		}
		return false
	}

	var a, b int
	fmt.Scanf("%d %d", &a, &b)
	c := []string{}
	for i := 0; i < a; i++ {
		tmp := ""
		fmt.Scanf("%s", &tmp)
		if mins(tmp) {
			c = append(c, tmp)
		}
	}

	res := []rune{}
	for i := 0; i < b; i++ {
		for ii := 0; ii < len(c); ii++ {
			tmp := c[ii][i]
			if tmp != '.' {
				res = append(res, rune(tmp))
			}
		}
	}
	fmt.Println(string(res))
}
