package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	valid := map[rune]bool{}
	for _, e := range input.Text() {
		if valid[e] {
			fmt.Println(0)
			return
		}
		valid[e] = true
	}
	fmt.Println(1)
}
