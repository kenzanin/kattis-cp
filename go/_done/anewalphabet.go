package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strings"
)

func main() {
	asciMap := [...]string{"@", "8", "(", "|)", "3", "#", "6", "[-]", "|",
		"_|", "|<", "1", "[]\\/[]", "[]\\[]", "0", "|D", "(,)", "|Z",
		"$", "']['", "|_|", "\\/", "\\/\\/", "}{", "`/", "2"}
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	var01 := input.Text()
	var01 = strings.ToLower(var01)
	res := bytes.Buffer{}
	for _, c := range var01 {
		if c >= 'a' && c <= 'z' {
			res.WriteString(asciMap[c-'a'])
		} else {
			res.WriteRune(c)
		}
	}
	fmt.Println(res.String())
}
