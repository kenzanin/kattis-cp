package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	input.Scan()
	b := strings.Split(input.Text(), " ")
	total := uint64(0)
	for _, e := range b {
		t, _ := strconv.ParseUint(e, 10, 64)
		total += t
	}
	if total%3 == 0 {
		fmt.Println("yes")
	} else {
		fmt.Println("no")
	}
}
