package main

import (
	"fmt"
	"math"
)

func main() {
	in1 := ""
	fmt.Scanf("%s", &in1)
	var t, g, c int
	for _, e := range in1 {
		switch e {
		case 'T':
			t++
			break
		case 'C':
			c++
			break
		case 'G':
			g++
			break
		}
	}
	min := func(a, b int) int {
		if a < b {
			return a
		}
		return b
	}
	powInt2 := func(a int) int {
		return int(math.Pow(float64(a), 2))
	}
	total := min(t, min(g, c)) * 7
	total += powInt2(t) + powInt2(g) + powInt2(c)
	fmt.Println(total)
}
