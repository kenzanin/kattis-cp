#include "bits/stdc++.h"

int main() {
  std::string in1;
  std::cin >> in1;
  std::unordered_map<char, int> data{{'b', 1}, {'k', 1}};
  for (auto const &e : in1) {
    if (data[e]) {
      data[e]++;
    }
  }

  if (data['b'] == 1 && data['k'] == 1) {
    std::cout << "none";
  } else if (data['b'] > data['k']) {
    std::cout << "boba\n";
  } else if (data['b'] == data['k']) {
    std::cout << "boki\n";
  } else {
    std::cout << "kiki\n";
  }
}