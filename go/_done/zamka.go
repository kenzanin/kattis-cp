package main

import (
	"fmt"
	"strconv"
)

func main() {
	a, b, c := 0, 0, 0
	fmt.Scan(&a, &b, &c)

	digits := func(a int) int {
		sum := 0
		for _, e := range strconv.Itoa(a) {
			sum += int(e - '0')
		}
		return sum
	}

	min, max := b, 0
	for i := a; i < b+1; i++ {
		if digits(i) == c {
			if min > i {
				min = i
			}
			if max < i {
				max = i
			}
		}
	}
	fmt.Print(min, "\n", max)
}
