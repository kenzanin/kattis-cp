package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	ada := input.Text()[len(input.Text())-3:]
	if ada == "eh?" {
		fmt.Println("Canadian!")
	} else {
		fmt.Println("Imposter!")
	}
}
