#include "arithmeticfunctions.h"
#include <bits/stdc++.h>

// Compute x^3
int cube(int x) { return std::pow(x, 3); }

// Compute the maximum of x and y
int max(int x, int y) { return std::max(x, y); }

// Compute x - y
int difference(int x, int y) { return std::abs(x - y); }
