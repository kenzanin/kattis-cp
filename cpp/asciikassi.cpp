#include "bits/stdc++.h"

void atas_bawah(const int in1) {
  std::cout << "+";
  for (int i = 0; i < in1; i++) {
    std::cout << "-";
  }
  std::cout << "+\n";
}

void kiri_kanan(const int in1) {
  for (int ii = 0; ii < in1; ii++) {
    std::cout << "|";
    for (int i = 0; i < in1; i++) {
      std::cout << " ";
    }
    std::cout << "|\n";
  }
}

int main() {
  int in1;
  std::cin >> in1;
  atas_bawah(in1);
  kiri_kanan(in1);
  atas_bawah(in1);
  return 0;
}