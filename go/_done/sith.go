package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	input.Scan()
	a, _ := strconv.Atoi(input.Text())
	input.Scan()
	b, _ := strconv.Atoi(input.Text())
	input.Scan()
	c, _ := strconv.Atoi(input.Text())

	a = a - b
	if a > 0 {
		fmt.Println("VEIT EKKI")
	} else {
		if a == c {
			fmt.Println("JEDI")
		} else {
			fmt.Println("SITH")
		}
	}

}
