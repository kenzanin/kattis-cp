#include "bits/stdc++.h"
#include <cstdio>

int main() {
  while (true) {
    int in1, in2;
    std::scanf("%d %d", &in1, &in2);
    if (in1 == 0) {
      return 0;
    }
    int a, b;
    a = in1 / in2;
    b = in1 % in2;
    std::printf("%d %d / %d\n", a, b, in2);
  }
}