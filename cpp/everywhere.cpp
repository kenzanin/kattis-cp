#include "bits/stdc++.h"

int main() {
  int n;
  std::cin >> n;
  for (int i = 0; i < n; i++) {
    int nn;
    std::cin >> nn;
    std::unordered_set<std::string> uset{};
    for (int ii = 0; ii < nn; ii++) {
      std::string in;
      std::cin >> in;
      uset.insert(in);
    }
    std::cout << uset.size() << "\n";
  }

  return 0;
}