package main

import "fmt"

func main() {
	in1 := 0
	fmt.Scanf("%d", &in1)
	for i := 0; i < in1; i++ {
		fmt.Printf("%d Abracadabra\n", i+1)
	}
}
