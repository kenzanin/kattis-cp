#include "bits/stdc++.h"

int main() {
  uint64_t a;
  std::cin >> a;
  if ((a & 1) == 0) {
    std::cout << "Bob\n";
  } else {
    std::cout << "Alice\n";
  }
}