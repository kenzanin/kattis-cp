package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	a := input.Text()
	input.Scan()
	b := input.Text()
	res := make([]byte, 0, len(b)/3)
	for i := 0; i <= len(b)-3; i += 3 {
		tmp := b[i : i+3]
		u, _ := strconv.Atoi(tmp)
		res = append(res, a[u-1])
	}
	fmt.Println(string(res))
}
