package main

import "fmt"

func main() {
	var in1, in2 uint64
	fmt.Scanf("%d\n%d", &in1, &in2)
	fmt.Println(in1 + in2)
}
