package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	a, b, c, d := 0, 0, 0, 0
	fmt.Scan(&a, &b)
	for i := 0; i < b; i++ {
		input.Scan()
		fmt.Sscan(input.Text(), &c)
		d += a - c
	}
	fmt.Println(d + a)
}
