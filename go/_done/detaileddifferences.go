package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	a := 0
	fmt.Sscan(input.Text(), &a)
	data := make([]string, 2)
	for i := 0; i < a; i++ {
		input.Scan()
		data[0] = input.Text()
		fmt.Println(data[0])
		input.Scan()
		data[1] = input.Text()
		fmt.Println(data[1])
		out := make([]rune, len(data[0]))
		for i := 0; i < len(data[0]); i++ {
			if data[0][i] == data[1][i] {
				out[i] = '.'
			} else {
				out[i] = '*'
			}
		}
		fmt.Println(string(out), "\n")
	}
}
