package main

import "fmt"

func main() {
	a, b := 0, 0
	fmt.Scan(&a, &b)
	if a > b {
		a, b = b, a
	}
	for i := a + 1; i <= b+1; i++ {
		fmt.Println(i)
	}
}
