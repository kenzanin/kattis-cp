#include "bits/stdc++.h"
int main() {
  auto convert = [](std::string &a) {
    std::string aa(a.rbegin(), a.rend());
    return std::atol(aa.c_str());
  };
  std::string a{}, b{};
  std::cin >> a >> b;
  std::cout << std::max(convert(a), convert(b)) << "\n";
}