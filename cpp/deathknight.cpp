#include "bits/stdc++.h"

int main() {
  int a;
  std::cin >> a;
  std::string data{};
  int tot{};
  for (int i = 0; i < a; i++) {
    std::cin >> data;
    if (data.find("CD") == std::string::npos) {
      tot++;
    }
  }
  std::cout << tot << "\n";
}