package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	tlight, dist := 0, 0
	fmt.Sscan(input.Text(), &tlight, &dist)
	time := 0
	a := 0
	for i := 0; i < tlight && time != dist; i++ {
		input.Scan()
		c, d, e := 0, 0, 0
		fmt.Sscan(input.Text(), &c, &d, &e)
		time += c - a
		a = c
		if time%(d+e) < d {
			time += d - time%(d+e)
		}
	}
	time += dist - a
	fmt.Println(time)
}
