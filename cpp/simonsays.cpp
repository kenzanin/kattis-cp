#include "bits/stdc++.h"

int main() {
  int a;
  std::cin >> a;
  std::cin.ignore();
  std::string in{};
  const std::string simon = "Simon says ";
  for (int i = 0; i < a; i++) {
    in.clear();
    std::getline(std::cin, in);
    if (in.substr(0, simon.length()) == simon) {
      std::cout << in.substr(simon.size()) << "\n";
    }
  }
}