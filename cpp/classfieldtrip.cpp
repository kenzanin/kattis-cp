#include "bits/stdc++.h"

int main() {
  std::string in1{}, in2{};
  std::cin >> in1 >> in2;
  in1 += in2;
  std::sort(in1.begin(), in1.end());
  std::cout << in1 << "\n";
  return 0;
}