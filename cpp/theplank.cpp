#include "bits/stdc++.h"

int main() {

  std::function<int(int)> variant = [&variant](int n) -> int {
    switch (n) {
    case 1:
      return 1;
    case 2:
      return 2;
    case 3:
      return 4;
    }
    return variant(n - 1) + variant(n - 2) + variant(n - 3);
  };

  int a;
  std::cin >> a;
  std::cout << variant(a) << "\n";
}
