#include "bits/stdc++.h"

int main() {
  std::string a;
  std::cin >> a;
  auto hiss = a.find("ss");
  std::cout << (hiss != a.npos ? "hiss" : "no hiss");
}