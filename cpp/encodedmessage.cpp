#include "bits/stdc++.h"
#include <cmath>
#include <cstdio>
#include <string>

int main() {
  int in1;
  std::cin >> in1;
  for (int i = 0; i < in1; i++) {
    std::string in2{};
    std::cin >> in2;
    const auto slen = in2.length();
    const auto step = std::sqrt(slen);
    std::string res{};
    int count = 0;
    for (int ii = step - 1; ii >= 0; ii--) {
      for (int iii = ii; iii <= slen - count; iii += step) {
        res += in2[iii];
      }
      count++;
    }
    std::cout << res << "\n";
  }
}