package main

import (
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	data, _ := io.ReadAll(os.Stdin)
	tmp := strings.Split(string(data), "\n")

	selisih := func(a, b uint64) uint64 {
		if a > b {
			return (a - b)
		}
		return b - a
	}

	for _, e := range tmp {
		if len(e) < 2 {
			continue
		}
		a, b := uint64(0), uint64(0)
		fmt.Sscanf(e, "%d %d", &a, &b)
		fmt.Println(selisih(a, b))
	}
}
