package main

import (
	"fmt"
)

func main() {
	a := 0
	fmt.Scan(&a)
	var b, c string
	fmt.Scan(&b, &c)
	datab := map[byte]int{}
	datac := map[byte]int{}
	var r, s int
	for i := 0; i < len(b); i++ {
		if b[i] == c[i] {
			r++
		} else {
			datab[b[i]]++
			datac[c[i]]++
		}
	}
	min := func(a, b int) int {
		if a < b {
			return a
		}
		return b
	}
	for e := range datab {
		s += min(datab[e], datac[e])
	}
	fmt.Printf("%d %d\n", r, s)
}
