package main

import (
	"fmt"
	"math/big"
)

func main() {
	a := int64(0)
	fmt.Scan(&a)
	aa := big.NewInt(a)
	mf := big.NewInt(1)
	for i := big.NewInt(2); i.Cmp(big.NewInt(128)) == -1; i.Add(i, big.NewInt(1)) {
		ii := big.NewInt(0)
		if (*big.Int).Cmp(aa, ii.Exp(i, big.NewInt(9), nil)) == 0 {
			mf = i
		}
	}
	fmt.Println(mf.Int64())
}
