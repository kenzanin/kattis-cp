package main

import (
	"fmt"
	"sort"
)

func main() {
	a := 0
	fmt.Scan(&a)
	books := make([]int, a)
	for i := range books {
		fmt.Scan(&books[i])
	}
	sort.Slice(books, func(i, j int) bool { return books[i] > books[j] })

	total := 0
	cap := 0
	for _, e := range books {
		if cap == 2 {
			cap = 0
			continue
		}
		total += e
		cap++
	}
	fmt.Println(total)
}
