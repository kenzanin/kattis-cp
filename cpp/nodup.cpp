#include "bits/stdc++.h"

int main() {
  std::vector<char> in2(80);
  auto size = std::cin.getline(in2.data(), in2.size()).gcount();
  in2.resize(size);

  char *token = std::strtok(in2.data(), " ");
  std::unordered_set<std::string> uset{};
  while (token != NULL) {
    if (uset.count(token) > 0) {
      std::cout << "no\n";
      return 0;
    };
    uset.insert(token);
    token = std::strtok(NULL, " ");
  }
  std::cout << "yes\n";
  return 0;
}