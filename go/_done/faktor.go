package main

import "fmt"

func main() {
	a, b := 0, 0
	fmt.Scan(&a, &b)
	fmt.Println(a*(b-1) + 1)
}
