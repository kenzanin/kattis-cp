#include "bits/stdc++.h"

int main() {
  std::string in1;
  std::cin >> in1;
  auto idx = in1.find('a');
  in1.erase(in1.begin(), in1.begin() + idx);
  std::cout << in1 << "\n";
  return 0;
}