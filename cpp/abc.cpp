#include "bits/stdc++.h"


int main() {
  std::vector<int> in1(3, 0);
  std::cin >> in1[0] >> in1[1] >> in1[2];
  std::sort(in1.begin(), in1.end());

  std::string in2{};
  std::cin >> in2;

  for (auto &e : in2) {
    std::cout << in1[e - 'A'] << " ";
  }

  return 0;
}