package main

import (
	"fmt"
)

func main() {
	data := ""
	multi := [...]int{4, 3, 2, 7, 6, 5, 0, 4, 3, 2, 1}
	fmt.Scan(&data)
	total := 0
	for i, e := range data {
		if e == '-' {
			continue
		}
		total += int(e-'0') * multi[i]
	}
	if total%11 == 0 {
		fmt.Println(1)
	} else {
		fmt.Println(0)
	}
}
