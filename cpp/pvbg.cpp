#include "bits/stdc++.h"

int main() {
  int in1;
  std::cin >> in1;
  int res = INT_MAX;
  for (int i = 0; i < in1; i++) {
    int tmp;
    std::cin >> tmp;
    res = (res > tmp) ? tmp : res;
  }
  std::cout << res + 1;
}