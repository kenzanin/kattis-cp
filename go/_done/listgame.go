package main

import (
	"fmt"
)

func main() {
	var a int
	fmt.Scan(&a)
	count := 0
	for i := 2; i*i <= a; i++ {
		for a%i == 0 {
			a /= i
			count++
		}
	}
	if a > 1 {
		count++
	}
	fmt.Println(count)
}
