package main

import "fmt"

func main() {
	a := 0
	fmt.Scan(&a)
	total := 0.0
	for i := 0; i < a; i++ {
		var b, c float64
		fmt.Scan(&b, &c)
		total += (b * c)
	}
	fmt.Printf("%.3f", total)
}
