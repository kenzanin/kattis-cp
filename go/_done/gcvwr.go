package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	a, b, c := 0, 0, 0
	fmt.Scan(&a, &b, &c)
	tmp := int(float64(a-b) * float64(0.9))
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	res := 0
	x := strings.Split(input.Text(), " ")
	for _, e := range x {
		xx, _ := strconv.Atoi(e)
		res += xx
	}
	fmt.Println(tmp - res)
}
