package main

import (
	"fmt"
	"math/big"
)

func main() {
	a, b := int64(0), int64(0)
	fmt.Scanf("%d/%d", &a, &b)
	aa := big.NewInt(5*a - 5*32*b)
	bb := big.NewInt(9 * b)
	cd := new(big.Int).GCD(nil, nil, aa, bb)
	a = aa.Div(aa, cd).Int64()
	b = bb.Div(bb, cd).Int64()
	fmt.Print(a, "/", b)
}
