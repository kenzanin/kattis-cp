package main

import "fmt"

func main() {
	var a, b int
	fmt.Scan(&a, &b)
	data := 0
	i := 0
	for ; i < a; i++ {
		tmp := 0
		fmt.Scan(&tmp)
		if (tmp + data) > b {
			break
		}
		data += tmp
	}
	fmt.Println(i)
}
