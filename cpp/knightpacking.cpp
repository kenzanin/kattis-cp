#include "bits/stdc++.h"

int main() {
  uint64_t a;
  std::cin >> a;
  std::cout << ((a & 1) == 0 ? "second" : "first")<<"\n";
}