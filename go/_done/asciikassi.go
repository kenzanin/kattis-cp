package main

import "fmt"

func atasBawah(in1 uint64) {
	data := make([]rune, in1)
	for i := range data {
		data[i] = '-'
	}

	fmt.Print("+")
	fmt.Printf("%s", string(data))
	fmt.Print("+\n")
}

func kananKiri(in1 uint64) {
	data := make([]rune, in1)

	for i := range data {
		data[i] = ' '
	}

	for ii := uint64(0); ii < in1; ii++ {
		fmt.Print("|")
		fmt.Printf("%s", string(data))
		fmt.Print("|\n")
	}
}

func main() {
	in1 := uint64(0)
	fmt.Scanf("%d", &in1)
	atasBawah(in1)
	kananKiri(in1)
	atasBawah(in1)
}
