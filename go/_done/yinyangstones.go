package main

import "fmt"

func main() {
	a := ""
	fmt.Scan(&a)
	bal := 0
	for _, e := range a {
		if e == 'W' {
			bal++
		} else {
			bal--
		}
	}
	if bal == 0 {
		fmt.Println(1)
	} else {
		fmt.Println(0)
	}
}
