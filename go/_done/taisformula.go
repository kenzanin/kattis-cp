package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	a := 0
	fmt.Sscan(input.Text(), &a)
	b := 0.0
	var c, d, cc, dd float64
	input.Scan()
	fmt.Sscan(input.Text(), &c, &d)
	for i := 0; i < a-1; i++ {
		input.Scan()
		fmt.Sscan(input.Text(), &cc, &dd)
		b += (((d + dd) * 0.5) * (cc - c)) * 0.001
		c = cc
		d = dd
	}
	fmt.Println(b)
}
