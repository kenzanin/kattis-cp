package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

func main() {
	a := 0
	fmt.Scan(&a)
	input := bufio.NewScanner(os.Stdin)
	data := []string{}
	shortest := 25
	for i := 0; i < a; i++ {
		input.Scan()
		b := input.Text()
		if len(b) < 5 {
			continue
		}
		unique := map[rune]bool{}
		for _, e := range b {
			if unique[e] == true {
				break
			}
			unique[e] = true
		}
		if len(unique) != len(b) {
			continue
		}

		if shortest > len(b) {
			data = []string{b}
			shortest = len(b)
		} else if shortest == len(b) {
			data = append(data, b)
		}
	}
	if len(data) == 0 {
		fmt.Println("Neibb")
	} else {
		sort.Strings(data)
		fmt.Println(data[len(data)-1])
	}
}
