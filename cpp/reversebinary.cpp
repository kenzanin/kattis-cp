#include "bits/stdc++.h"

int main() {
  uint64_t in1{};
  std::cin >> in1;
  uint64_t res{};
  for (; in1 > 0; in1 >>= 1) {
    res <<= 1;
    if (in1 & 1) {
      res |= 1;
    }
  };
  std::cout << res << "\n";
  return 0;
}