#include "bits/stdc++.h"
#include <algorithm>

int main() {
  int a;
  std::cin >> a;
  std::vector<int> aa(a, 0);
  std::for_each(aa.begin(), aa.end(), [](auto &e) { std::cin >> e; });
  std::vector<int> cc(aa.begin(), aa.end());
  std::sort(cc.begin(), cc.end());

  auto [it1, it2] = std::mismatch(cc.begin(), cc.end(), aa.begin());
  int count = 0;
  while (it1 != cc.end()) {
    ++count;
    std::advance(it1, 1);
    std::advance(it2, 1);
    std::tie(it1, it2) = std::mismatch(it1, cc.end(), it2);
  }

  std::cout << count << "\n";
}