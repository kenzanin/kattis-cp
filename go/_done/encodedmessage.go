package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	in1, _ := strconv.Atoi(input.Text())
	for i := 0; i < in1; i++ {
		input.Scan()
		in2 := input.Text()
		slen := len(in2)
		step1 := int(math.Sqrt(float64(slen)))
		if step1 == 1 {
			fmt.Println(in2)
			continue
		}
		res := ""
		count := 0
		for ii := step1 - 1; ii >= 0; ii-- {
			for iii := ii; iii <= slen-count; iii += step1 {
				res += string(in2[iii])
			}
			count++
		}
		fmt.Printf("%s\n", string(res))
	}
}
