package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	a := 0
	fmt.Sscan(input.Text(), &a)
	unique := map[int]bool{}
	var b, c int
	for i := 0; i < a; i++ {
		input.Scan()
		fmt.Sscan(input.Text(), &b, &c)
		for ii := b; ii <= c; ii++ {
			if unique[ii] {
				continue
			}
			unique[ii] = true
		}
	}
	fmt.Println(len(unique))
}
