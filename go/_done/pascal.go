package main

import (
	"fmt"
	"math"
)

func main() {
	a := 0
	fmt.Scan(&a)
	for i := 2; i < int(math.Sqrt(float64(a)))+1; i++ {
		if a%i == 0 {
			tmp := a / i
			fmt.Println(a - tmp)
			return
		}
	}
	fmt.Println(a - 1)
}
