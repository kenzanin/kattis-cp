#include "bits/stdc++.h"

int main() {
  long double a, b, c;
  std::cin >> a >> b >> c;
  if (c == 1) {
    std::cout << std::setprecision(0) << std::fixed << a * b;
  } else {
    __float128 d = a * b / c;
    long double e = d;
    std::cout << std::setprecision(20) << std::fixed << e;
  }
}