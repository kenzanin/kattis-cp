#include "bits/stdc++.h"

int main() {
  int in1;
  std::cin >> in1;
  std::vector<std::string> data(in1);
  int maxlen{};
  std::string res{};
  for (auto &e : data) {
    std::cin >> e;
    maxlen = maxlen > e.length() ? maxlen : e.length();
  }
  for (int i = 0; i < maxlen; i++) {
    int avg{}, entry{};
    for (int ii = 0; ii < in1; ii++) {
      if (i < data[ii].length()) {
        entry++;
        avg += data[ii][i];
      }
    }
    avg /= entry;
    res += avg;
  }
  std::cout << res << "\n";
}