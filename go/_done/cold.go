package main

import "fmt"

func main() {
	a := 0
	fmt.Scanln(&a)
	res := 0
	for i := 0; i < a; i++ {
		b := 0
		fmt.Scan(&b)
		if b < 0 {
			res++
		}
	}
	fmt.Println(res)
}
