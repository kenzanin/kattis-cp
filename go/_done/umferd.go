package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	var a, b int
	fmt.Scan(&a, &b)
	scanner := bufio.NewScanner(os.Stdin)
	d := 0.0
	for ii := 0; ii < b; ii++ {
		scanner.Scan()
		c := scanner.Text()
		f := 0
		for _, e := range c {
			if e == '.' {
				f++
			}
		}
		d += float64(f) / float64(a*b)
	}
	fmt.Println(d)
}
