package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	a := 0
	fmt.Scan(&a)
	input := bufio.NewScanner(os.Stdin)
	data := make([]int, a)
	for i := 0; i < a; i++ {
		input.Scan()
		data[i], _ = strconv.Atoi(input.Text())
	}

	for i, j := 0, len(data)-1; i < j; i, j = i+1, j-1 {
		data[i], data[j] = data[j], data[i]
	}

	for _, e := range data {
		fmt.Println(e)
	}

}
