#include "bits/stdc++.h"

int main() {
  std::vector<std::vector<std::string>> data{};
  for (; true;) {
    int in1{};
    std::cin >> in1;
    if (in1 == 0) {
      break;
    }
    std::vector<std::string> tmp(in1);
    for (int i = 0; i < in1; i++) {
      std::cin >> tmp[i];
    }
    data.push_back(tmp);
  }

  for (int ii = 0; ii < data.size(); ii++) {
    std::vector<std::string> tmp(data[ii].size());
    int head = 0, tail = data[ii].size() - 1;
    for (int i = 1; i < data[ii].size() + 1; i++) {
      if ((i & 1) == 1) {
        tmp.at(head) = data[ii][i - 1];
        head++;
      } else {
        tmp.at(tail) = data[ii][i - 1];
        tail--;
      }
    }
    std::cout << "SET " << ii + 1 << "\n";
    for (auto &e : tmp) {
      std::cout << e << "\n";
    }
  }
}