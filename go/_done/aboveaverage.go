package main

import "fmt"

func main() {
	in1 := 0
	fmt.Scanf("%d", &in1)
	for i := 0; i < in1; i++ {
		in2 := 0
		fmt.Scanf("%d", &in2)
		data := make([]float64, in2)
		avg := 0.0
		for ii := 0; ii < in2; ii++ {
			fmt.Scanf("%f", &data[ii])
			avg += data[ii]
		}
		avg /= float64(in2)
		percent := 0.0
		for ii := 0; ii < in2; ii++ {
			if data[ii] > avg {
				percent++
			}
		}
		fmt.Printf("%0.3f%%\n", percent/float64(in2)*100.0)
	}
}
