#include "bits/stdc++.h"

int main() {
  int in1;
  std::cin >> in1;
  double avg = 0.0;
  int count = 0;
  for (int i = 0; i < in1; i++) {
    int in2;
    std::cin >> in2;
    if (in2 < 0) {
      continue;
    }
    avg += in2;
    count++;
  }
  avg /= count;
  char buf[40]{};
  std::sprintf(buf, "%.17f", avg);
  std::string res(buf);
  auto dotpos = res.find('.') + 1;
  for (int i = res.length()-1; i > dotpos; i--) {
    if (res[i] < '5') {
      res.erase(i);
    } else {
      break;
    }
  }
  std::cout << res << "\n";
}