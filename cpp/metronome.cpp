#include "bits/stdc++.h"

int main() {
  int in;
  std::cin >> in;
  std::cout << std::showpoint << std::fixed
            << std::setprecision((in % 4) == 0 ? 1 : 2)
            << static_cast<float>(in / 4.0) << "\n";
}