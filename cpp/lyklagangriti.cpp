#include "bits/stdc++.h"

int main() {
  std::string a;
  std::getline(std::cin, a);
  std::list<char> data{};
  auto it = data.begin();
  for (auto const &e : a) {
    switch (e) {
    case 'L': {
      it = std::prev(it, 1);
    } break;
    case 'R': {
      it = std::next(it, 1);
    } break;
    case 'B': {
      it = data.erase(it);
      it = std::prev(it, 1);
    } break;
    default:
      it = std::next(it, 1);
      it = data.insert(it, e);
    }
  }
  std::cout << std::string(data.begin(), data.end()) << "\n";
}