package main

import (
	"fmt"
	"math"
)

func main() {
	a := 0
	fmt.Scanln(&a)
	for i := 0; i < a; i++ {
		b := 0
		fmt.Scanln(&b)
		bb := int(math.Abs(float64(b)))
		if (bb & 1) == 0 {
			fmt.Println(b, "is even")
		} else {
			fmt.Println(b, "is odd")
		}
	}
}
