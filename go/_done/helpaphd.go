package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	a := 0
	fmt.Scan(&a)
	input := bufio.NewScanner(os.Stdin)
	b, c := 0, 0
	for i := 0; i < a; i++ {
		input.Scan()
		ii, _ := fmt.Sscanf(input.Text(), "%d+%d", &b, &c)
		if ii == 0 {
			fmt.Println("skipped")
			continue
		}
		fmt.Println(b + c)
	}
}
