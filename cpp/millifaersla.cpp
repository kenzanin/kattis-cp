#include "bits/stdc++.h"

int main() {
  std::multimap<int, std::string> data{};
  for (auto &e : std::array<const std::string, 3>{"Monnei", "Fjee",
                                                  "Dolladollabilljoll"}) {
    int tmp{};
    std::cin >> tmp;
    data.insert(std::make_pair(tmp, e));
  }
  std::cout << data.begin()->second << "\n";
}