import decimal

def main():
    data = input()
    a, b, c = data.split()
    d = decimal.Decimal(a) * decimal.Decimal(b)
    if c == "1":
        if d >= 1e9:
            print(format(d, ".18f"))
        else:
            print(d)
    else:
        decimal.getcontext().prec = 35
        print(format(d / decimal.Decimal(c), ".18f"))

if __name__=="__main__":
    main()