#include <bits/stdc++.h>

int main() {
    uint64_t a, b, c;
    std::cin >> a >> b >> c;
    std::vector<uint64_t> data(a);
    for (auto &e : data) {
        std::cin >> e;
        e *= b;
    }
    std::sort(data.begin(), data.end());
    uint64_t sum = 0;
    uint64_t count = 0;
    while (count < a && sum + data[count] <= (count + 1) * c) {
        sum += data[count];
        ++count;
    }
    std::cout << count << "\n";
}
