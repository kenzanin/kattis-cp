#include "bits/stdc++.h"

namespace {
const double E1 = 60.0;
const double E2 = 11.0;
const double Harga1 = 5.0;
const double Harga2 = 60.0;
} // namespace
 
int main() {
  int H, P;
  std::cin >> H >> P;
  double k1 = Harga1, k2 = Harga2;
  int days{};
  int runHour{};
  do {
    k1 += (E1 * H * P) * 0.00001;
    k2 += (E2 * H * P) * 0.00001;
    runHour += H;
    if (runHour > 1000) {
      k1 += Harga1;
      runHour -= 1000;
    }
    days++;
  } while (k2 > k1);
  std::cout << days << "\n";
}