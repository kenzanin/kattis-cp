package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// Data is ...
var Data struct {
	id   int
	menu string
}

func main() {
	input := bufio.NewScanner(os.Stdin)
	a := 0
	fmt.Scan(&a)
	data := Data
	for i := 0; i < a; i++ {
		tmp := 0
		fmt.Scan(&tmp)
		input.Scan()
		tmp1 := input.Text()
		if data.id < tmp {
			data.id = tmp
			data.menu = tmp1
		}
	}
	fmt.Println(data.id)
	menu := strings.Split(data.menu, " ")
	for i := len(menu) - 1; i >= 0; i-- {
		fmt.Println(menu[i])
	}
}
