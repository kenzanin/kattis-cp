package main

import (
	"fmt"
)

func main() {
	vowels := map[rune]bool{'a': true, 'e': true, 'i': true, 'o': true, 'u': true, 'y': true}
	for {
		var a int
		fmt.Scanf("%d", &a)
		if a == 0 {
			break
		} else if a == 1 {
			in := ""
			fmt.Scanf("%s", &in)
			for i := 0; i < len(in); i++ {
				if vowels[rune(in[i])] {
					fmt.Printf("%s\n", in)
				}
			}
		} else {
			maxPair := 0
			maxStr := ""
			for i := 0; i < a; i++ {
				var in string
				fmt.Scanf("%s", &in)
				pair := 0
				for ii := 0; ii < len(in)-1; ii++ {
					if (in[ii] == in[ii+1]) && vowels[rune(in[ii])] {
						pair++
						ii++
					}
				}
				if pair > maxPair {
					maxPair = pair
					maxStr = in
				}
			}
			fmt.Println(maxStr)
		}
	}
}
