package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func aiueo(in rune) int {
	switch in {
	case 'a', 'i', 'u', 'e', 'o':
		return 1
	}
	return 0
}

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	var01 := input.Text()
	var01 = strings.ToLower(var01)
	res := 0
	for _, e := range var01 {
		res += aiueo(e)
	}
	fmt.Printf("%d\n", res)
}
