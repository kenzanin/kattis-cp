#include "bits/stdc++.h"
#include <string>

int main() {
  std::array<const char *, 26> map{
      "@",  "8",    "(",   "|)",      "3",      "#",  "6",  "[-]", "|",
      "_|", "|<",   "1",   "[]\\/[]", "[]\\[]", "0",  "|D", "(,)", "|Z",
      "$",  "']['", "|_|", "\\/",     "\\/\\/", "}{", "`/", "2"};
  std::string in1{};
  std::getline(std::cin, in1);

  std::string res{};
  for (auto const &e : in1) {
    int tmp = std::tolower(e);
    if (tmp >= 'a' && tmp <= 'z') {
      res += map[tmp - 'a'];
    } else {
      res += e;
    }
  }
  std::cout << res;
  return 0;
}