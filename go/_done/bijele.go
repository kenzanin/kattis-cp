package main

import "fmt"

func main() {
	valid := [...]int{1, 1, 2, 2, 2, 8}
	a := 0
	for _, e := range valid {
		fmt.Scan(&a)
		fmt.Print(e-a, " ")
	}
	fmt.Print("\n")
}
