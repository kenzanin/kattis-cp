#include "bits/stdc++.h"

int main() {
  auto s1 = [](int s) {
    s++;
    return (s * (s - 1)) / 2;
  };

  auto s2 = [](int s) {
    int res{};
    for (int i = 1; i < 2 * s; i += 2) {
      res += i;
    }
    return res;
  };

  auto s3 = [](int s) {
    int res{};
    for (int i = 0; i < (2 * s) + 1; i += 2) {
      res += i;
    }
    return res;
  };

  int a;
  std::cin >> a;
  for (int i = 0; i < a; i++) {
    int var;
    std::cin >> var >> var;
    std::cout << i + 1 << " " << s1(var) << " " << s2(var) << " " << s3(var)
              << "\n";
  }
}