#include "bits/stdc++.h"
#include <cstdint>

int main() {
  int a, b, c;
  std::cin >> a >> b >> c;
  std::cout << static_cast<int64_t>(a * b * c);
}