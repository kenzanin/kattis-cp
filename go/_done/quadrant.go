package main

import "fmt"

func main() {
	var a, b int
	fmt.Scanf("%d\n%d", &a, &b)
	kuat := 0
	if a < 0 {
		if b < 0 {
			kuat = 3
		} else {
			kuat = 2
		}
	} else {
		if b < 0 {
			kuat = 4
		} else {
			kuat = 1
		}
	}
	fmt.Println(kuat)
}
