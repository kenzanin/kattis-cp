#include "bits/stdc++.h"

int main() {
  int a;
  std::cin >> a;
  for (int i = 0; i < a; i++) {
    int b{};
    double c{};
    std::scanf("%d %lf", &b, &c);
    auto bpm = (60.0 * b) / c;
    auto pm = 60.0 / c;
    std::cout << std::fixed << std::setprecision(4) << bpm - pm << " " << bpm
              << " " << bpm + pm << "\n";
  }
}