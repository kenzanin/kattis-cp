#include "bits/stdc++.h"

int main() {
  int64_t in1, in2;
  std::cin >> in1 >> in2;
  std::vector<int64_t> data(in1, 0);
  for (auto &e : data) {
    std::cin >> e;
  }
  switch (in2) {
  case 1: {
    for (int i = 0; i < data.size() / 2; i++) {
      auto tmp = 7777 - data[i];
      auto ada = std::find(data.begin() + i, data.end(), tmp);
      if (ada != data.end()) {
        std::cout << "Yes";
        return 0;
      }
    }
    std::cout << "No\n";
  } break;
  case 2: {
    auto tmp = std::unique(data.begin(), data.end());
    std::cout << (tmp == data.end() ? "Unique\n" : "Contains duplicate\n");
  } break;
  case 3: {
    auto ntimes = std::floor(in1 / 2.0);
    std::unordered_map<int64_t, int64_t> tmp{};
    for (auto &e : data) {
      tmp[e]++;
      if (tmp.at(e) > ntimes) {
        std::cout << e << "\n";
        return 0;
      }
    }
    std::cout << -1 << "\n";
  } break;
  case 4: {
    std::sort(data.begin(), data.end());
    int tmp = data.size() / 2;
    if ((in1 & 1) == 0) {
      std::cout << data[tmp - 1] << " " << data[tmp];
    } else {
      std::cout << data[tmp];
    }
  } break;
  case 5: {
    std::list<int64_t> tmp{};
    for (auto &e : data) {
      if (e >= 100 && e <= 999) {
        tmp.push_back(e);
      }
    }
    tmp.sort();
    for (auto &e : tmp) {
      std::cout << e << " ";
    }
  } break;
  default:
    break;
  };
}