package main

import (
	"fmt"
	"sort"
)

type sortIt []rune

func (a sortIt) Len() int           { return len(a) }
func (a sortIt) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a sortIt) Less(i, j int) bool { return a[i] < a[j] }

func main() {
	in1 := ""
	in2 := ""
	fmt.Scanf("%s\n%s", &in1, &in2)
	in1 += in2

	dat := []rune(in1)
	sort.Sort(sortIt(dat))
	fmt.Printf("%s\n", string(dat))
}
