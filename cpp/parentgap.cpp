#include "bits/stdc++.h"

const std::time_t getx(int year, int month, int wday, int wdayke) {
  std::tm tmp{0, 0, 0, 1, month - 1, year - 1900};
  auto test = mktime(&tmp);
  if (tmp.tm_wday != wday) {
    tmp.tm_mday += (7 - wday) - tmp.tm_wday;
  }
  tmp.tm_mday += 7 * (wdayke - 1);
  return mktime(&tmp);
}

int main() {
  int in1;
  std::cin >> in1;
  auto const ibu = getx(in1, 5, 0, 2);
  auto const ayah = getx(in1, 6, 0, 3);
  auto const selisih = std::difftime(ayah, ibu);
  std::cout << selisih / (7 * 24 * 60 * 60) << " weeks\n";
}