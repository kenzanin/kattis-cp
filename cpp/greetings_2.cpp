#include "bits/stdc++.h"

int main() {
  std::string in1{};
  std::cin >> in1;

  for (int i = 0; i < in1.size(); i++) {
    if (in1[i] == 'e') {
      std::cout << "ee";
    } else {
      std::cout << in1[i];
    }
  }
  std::cout << "\n";
}