#include "bits/stdc++.h"
#include <cstdint>

int main() {
  int in1;
  std::cin >> in1;
  int64_t res{};
  for (int i = 0; i < in1; i++) {
    int in2;
    std::cin >> in2;
    res += in2;
  }
  std::cout << res << "\n";
}