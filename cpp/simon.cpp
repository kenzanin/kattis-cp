#include "bits/stdc++.h"

const std::string_view simon = "simon says ";

int main() {
  int a;
  std::cin >> a;
  std::cin.ignore();
  for (int i = 0; i < a; i++) {
    std::string a;
    std::getline(std::cin, a);
    auto cmd = a.substr(0, simon.length()) == simon;
    if (cmd) {
      std::cout << a.substr(simon.length()) << "\n";
    }
  }
}