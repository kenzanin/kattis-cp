package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	mmap := map[rune]int{'.': 20, 'O': 10, '\\': 25, '/': 25, 'A': 35, '^': 5, 'v': 22}
	a, b := 0, 0
	fmt.Scan(&a, &b)
	input := bufio.NewScanner(os.Stdin)
	total := uint64(0)
	for i := 0; i < a; i++ {
		input.Scan()
		for _, e := range input.Text() {
			total += uint64(mmap[e])
		}
	}
	fmt.Print(total)
}
