#include "bits/stdc++.h"

int main() {
  long long int a, b;
  std::cin >> a >> b;
  auto tmp = a % b;
  std::cout << std::min(tmp, b - tmp);
}