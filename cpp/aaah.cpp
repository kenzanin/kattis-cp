#include "bits/stdc++.h"

int main() {
  std::string in1{}, in2{};
  std::getline(std::cin, in1);
  std::getline(std::cin, in2);
  std::cout << (in1.length() < in2.length() ? "no" : "go") << "\n";
  return 0;
}
