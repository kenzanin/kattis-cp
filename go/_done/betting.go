package main

import "fmt"

func main() {
	a := 0.0
	fmt.Scan(&a)
	fmt.Printf("%.10f\n%.10f", 100.0/a, 100.0/(100.0-a))
}
