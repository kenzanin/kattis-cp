package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	a := input.Text()
	res := []rune{}
	for _, e := range a {
		if len(res) == 0 || res[len(res)-1] != e {
			if e == ' ' {
				res = append(res, ' ')
			} else {
				res = append(res, e)
			}
		}
	}
	fmt.Println(string(res))
}
