#include "bits/stdc++.h"

int main() {
  int a, b;
  std::cin >> a >> b;
  double avg{}, min{}, max{};
  for (int i = 0; i < b; i++) {
    int tmp;
    std::cin >> tmp;
    avg += tmp;
  }
  min = avg;
  max = avg;
  for (int i = 0; i < a - b; i++) {
    min += -3;
    max += 3;
  }
  std::cout << min / a << " " << max / a << "\n";
}