#include "bits/stdc++.h"

int main() {
  int in1{};
  std::cin >> in1;
  while (in1--) {
    int in2;
    std::cin >> in2;
    std::vector<int> nilai(in2, 0);
    for (auto &e : nilai) {
      std::cin >> e;
    }
    const double avg = std::reduce(nilai.begin(), nilai.end(), 0.0) /
                       static_cast<double>(nilai.size());
    double percent{};
    for (const auto &e : nilai) {
      percent += (e > avg ? 1.0 : 0.0);
    }

    percent = (percent / static_cast<double>(nilai.size())) * 100.0;
    std::cout << std::fixed << std::setprecision(3) << percent << "%"
              << "\n";
  }
  return 0;
}