#include "bits/stdc++.h"
#include <algorithm>
#include <cstdio>
#include <iterator>
#include <string>

int main() {
  double in1;
  std::cin >> in1;
  auto tmp = in1 * 0.09144;
  char buf[100]{};
  std::sprintf(buf, "%.14f", tmp);
  std::string res(buf);
  res.resize(res.length());
  auto dot = res.find(".");
  for (int i = dot + 14; i > dot; i--) {
    if (res[i] == '0') {
      res.erase(i);
    } else {
      break;
    }
  }
  std::cout << res;
}