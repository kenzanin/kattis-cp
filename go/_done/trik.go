package main

import "fmt"

func main() {

	geser2 := map[int]map[rune]int{1: {'A': 2, 'C': 3}, 2: {'A': 1, 'B': 3}, 3: {'B': 2, 'C': 1}}

	a := ""
	fmt.Scan(&a)
	pos := 1
	for _, e := range a {
		v, e := geser2[pos][e]
		if e {
			pos = v
		}
	}
	fmt.Println(pos)
}
