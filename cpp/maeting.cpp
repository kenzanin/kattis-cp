#include "bits/stdc++.h"

int main() {
  int a;
  std::cin >> a;
  std::unordered_map<std::string, int> student{};
  std::string name;
  for (int i = 0; i < a; i++) {
    std::cin >> name;
    student[name] = 0;
  }
  std::cin >> a;
  for (int i = 0; i < a; i++) {
    int b;
    std::cin >> b;
    for (int ii = 0; ii < b; ii++) {
      std::cin >> name;
      student[name]++;
    }
  }
  std::vector<std::pair<int, std::string>> sstudent{};
  for (auto const &e : student) {
    sstudent.push_back(std::make_pair(e.second, e.first));
  }
  std::sort(sstudent.begin(), sstudent.end(),
            [](std::pair<int, std::string> a, std::pair<int, std::string> b) {
              return a.first > b.first;
            });
  for (auto const &e : sstudent) {
    std::cout << e.first << " " << e.second << "\n";
  }
}
