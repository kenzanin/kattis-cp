package main

import "fmt"

func main() {
	var a, b int
	fmt.Scan(&a, &b)
	for i := 0; i < b; i++ {
		c := ""
		d := 0
		fmt.Scan(&c, &d)
		fmt.Printf("%s ", c)
		if d > a {
			fmt.Print("opin\n")
		} else {
			fmt.Print("lokud\n")
		}
	}
}
