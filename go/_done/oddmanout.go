package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	a := 0
	fmt.Scan(&a)
	for i := 0; i < a; i++ {
		check := map[uint64]int{}
		input.Scan()
		input.Scan()
		b := strings.Split(input.Text(), " ")
		c := uint64(0)
		for _, e := range b {
			fmt.Sscan(e, &c)
			check[c]++
		}
		for ee, e := range check {
			if e == 1 {
				fmt.Print("Case #", i+1, ": ", ee)
				break
			}
		}
		fmt.Print("\n")
	}
}
