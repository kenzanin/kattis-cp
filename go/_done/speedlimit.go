package main

import "fmt"

func main() {
	for {
		a := 0
		fmt.Scanf("%d", &a)
		if a == -1 {
			break
		}
		distant := 0
		dura := 0
		for i := 0; i < a; i++ {
			var c, d int
			fmt.Scanf("%d %d", &c, &d)
			distant += c * (d - dura)
			dura = d
		}
		fmt.Printf("%d miles\n", distant)
	}
}
