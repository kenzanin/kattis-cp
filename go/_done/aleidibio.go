package main

import "fmt"

func main() {
	var a, b, c uint64
	fmt.Scanf("%d\n%d\n%d", &a, &b, &c)
	fmt.Printf("%d\n", c-a-b)
}
