#include "bits/stdc++.h"

int main() {
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  double a{};
  std::cin >> a;
  int b{};
  std::cin >> b;
  double e{};
  char x{};
  for (int i = 0; i < b; i++) {
    std::cin >> x;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    if (x == 'e') {
      e++;
    }
  }
  double p = e / b;
  std::cout << ((p > a) ? "Neibb" : "Jebb") << "\n";
}