#include "bits/stdc++.h"

int main() {
  std::string in1;
  std::cin >> in1;
  int t{}, g{}, c{};
  for (const auto &e : in1) {
    switch (e) {
    case 'T': {
      t++;
    } break;
    case 'G': {
      g++;
    } break;
    case 'C': {
      c++;
    } break;
    default:
      break;
    }
  }

  auto min = [](int a, int b) { return (a < b ? a : b); };
  uint32_t sets = min(c, min(g, t));
  sets *= 7;
  std::cout << std::pow(t, 2) + std::pow(g, 2) + std::pow(c, 2) + sets << "\n";
}