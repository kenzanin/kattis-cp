package main

import (
	"fmt"
	"sort"
)

func main() {
	var menit, jsong int
	fmt.Scanf("%d %d", &menit, &jsong)
	esong := []int{}
	for i := 0; i < jsong; i++ {
		tmp := 0
		fmt.Scanf("%d", &tmp)
		esong = append(esong, tmp)
	}
	sort.Slice(esong, func(i, j int) bool {
		return esong[i] < esong[j]
	})
	res := 0
	for _, e := range esong {
		if (res + e) > menit*60 {
			break
		}
		res += e
	}
	fmt.Println(res)
}
