package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	valid := func(a int) bool {
		s := strconv.Itoa(a)
		if strings.Contains(s, "0") {
			return false
		}

		for i := 0; i < len(s)-1; i++ {
			for ii := i + 1; ii < len(s); ii++ {
				if s[i] == s[ii] {
					return false
				}
			}
		}

		for _, e := range s {
			ee := int(e - '0')
			if (a % ee) != 0 {
				return false
			}
		}
		return true
	}

	var a, b int
	fmt.Scan(&a, &b)
	count := 0
	for i := a; i < b; i++ {
		if valid(i) {
			count++
		}
	}
	fmt.Println(count)
}
