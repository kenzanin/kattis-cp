package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b, c, d float64
	fmt.Scan(&a, &b, &c, &d)
	area := math.Abs(a-c) * math.Abs(b-d)
	fmt.Printf("%.3f", area)
}
