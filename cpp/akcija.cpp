#include "bits/stdc++.h"

int main() {
  int in1;
  std::cin >> in1;
  auto data = std::multiset<int, std::greater<int>>{};
  for (int i = 0; i < in1; i++) {
    int var;
    std::cin >> var;
    data.insert(var);
  }

  int skip = 0;
  uint64_t total = 0;
  for (auto const &e : data) {
    skip++;
    if (skip == 3) {
      skip = 0;
      continue;
    }
    total += e;
  }
  std::cout << total;
  return 0;
}