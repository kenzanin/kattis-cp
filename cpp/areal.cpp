#include "bits/stdc++.h"
#include <cmath>
#include <iomanip>
#include <ios>

int main() {
  double in1;
  std::cin >> in1;
  in1 = std::sqrt(in1) * 4.0;
  if (std::floor(in1) == in1) {
    std::cout << std::fixed << std::setprecision(0) << in1 << "\n";
  } else {
    std::cout << std::fixed << std::setprecision(20) << in1 << "\n";
  }
  return 0;
}