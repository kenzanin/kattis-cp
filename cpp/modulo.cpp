#include "bits/stdc++.h"

int main() {
  std::unordered_set<int> mod{};
  for (int i = 0; i < 10; i++) {
    int var01;
    std::cin >> var01;
    mod.insert(var01 % 42);
  }
  std::cout << mod.size() << "\n";
  return 0;
}