#include "bits/stdc++.h"

int main() {
  std::string in1;
  std::cin >> in1;
  in1 = std::string(in1.begin(), in1.begin() + 3);
  std::cout << (in1 == "555" ? "1" : "0");
  return 0;
}