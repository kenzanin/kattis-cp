package main

import "fmt"

func main() {
	var a, b int
	fmt.Scan(&a, &b)

	switch {
	case a == 0 && b == 0:
		{
			fmt.Print("Not a moose")
		}
	case a == b:
		{
			fmt.Print("Even ", a+b)
		}
	case a > b:
		{
			fmt.Print("Odd ", a*2)
		}
	case b > a:
		{
			fmt.Print("Odd ", b*2)
		}
	default:
		fmt.Print("Not a moose")
	}
}
