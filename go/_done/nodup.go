package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	var01 := input.Text()
	var01 = strings.ToLower(var01)

	data1 := strings.Split(var01, " ")
	data := map[string]bool{}
	for _, e := range data1 {
		if data[e] == true {
			fmt.Printf("no\n")
			return
		}
		data[e] = true
	}
	fmt.Printf("yes\n")
}
