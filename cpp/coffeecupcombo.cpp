#include "bits/stdc++.h"

int main() {
  int in1{}, res{};
  std::string in2{};
  std::cin >> in1 >> in2;
  int var01{};
  for (int i = 0; i < in1; i++) {
    if (in2[i] == '1') {
      var01 = 2;
      res++;
    } else if (var01 > 0) {
      var01--;
      res++;
    }
  }
  std::cout << res << "\n";
}