#include "bits/stdc++.h"

const std::array<int, 14> proba{0, 0, 1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1};

int main() {
  int in1;
  std::cin >> in1;
  double res{};
  for (int i = 0; i < in1; i++) {
    int in2;
    std::cin >> in2;
    res += proba[in2];
  }
  res /= 36.0;
  auto point = res == std::floor(res);
  std::cout << std::fixed << std::setprecision(point ? 1 : 17) << res;
}