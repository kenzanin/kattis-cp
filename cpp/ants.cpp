#include <bits/stdc++.h>

int main() {
  int n;
  std::cin >> n;
  for (int i = 0; i < n; i++) {
    int a, b;
    std::cin >> a >> b;
    std::vector<int> data(b, 0);
    for (auto &e : data) {
      std::cin >> e;
    }
    auto c = std::min(data[0], a - data[0]);
    auto d = std::max(data[0], a - data[0]);
    for (auto &e : data) {
      c = std::max(c, std::min(e, a - e));
      d = std::max(d, std::max(e, a - e));
    }
    std::cout << c << " " << d << "\n";
  }
}
