package main

import (
	"fmt"
)

type resType struct {
	status bool
	score  int
}

func main() {
	data := map[rune]resType{}
	for in1 := 0; in1 != -1; {
		in2 := ' '
		in3 := ""
		fmt.Scanf("%d %c %s", &in1, &in2, &in3)
		if in1 == -1 {
			break
		}
		if in3 == "right" {
			data[in2] = resType{true, data[in2].score + in1}
		} else {
			data[in2] = resType{data[in2].status, data[in2].score + 20}
		}
	}
	total01 := 0
	total02 := 0
	for _, ee := range data {
		if ee.status == true {
			total01++
			total02 += ee.score
		}
	}
	fmt.Printf("%d %d\n", total01, total02)
}
