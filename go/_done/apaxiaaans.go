package main

import (
	"fmt"
)

func main() {
	a := ""
	fmt.Scan(&a)
	res := []rune{}
	for _, e := range a {
		if len(res) == 0 || res[len(res)-1] != e {
			res = append(res, e)
		}
	}
	fmt.Println(string(res))
}
