#include "bits/stdc++.h"

int main() {
  std::string in1{}, in2{};
  std::cin >> in1 >> in2;

  while (in1.back() == '0' && in2.back() == '0') {
    in1.pop_back();
    in2.pop_back();
  }

  if (in1.size() < in2.size()) {
    auto fzero = in2.size() - in1.size();
    in1.insert(0, fzero, '0');
  }

  // put the point
  in1.insert(in1.length() - in2.length() + 1, ".");
  while (in1.back() == '0' || in1.back() == '.') {
    if (in1.back() == '.') {
      in1.pop_back();
      break;
    }
    in1.pop_back();
  }
  std::cout << in1;
}