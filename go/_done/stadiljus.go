package main

import (
	"fmt"
	"sort"
)

func main() {
	var a int64
	var b, c int32
	fmt.Scan(&a, &b, &c)
	data := make([]int32, a)
	for i := range data {
		fmt.Scan(&data[i])
		data[i] *= b
	}
	sort.Slice(data, func(i, j int) bool { return data[i] < data[j] })
	var count int64
	var sum int64
	for (count < a) && (sum+int64(data[count]) <= (count+1)*int64(c)) {
		sum += int64(data[count])
		count++
	}
	fmt.Println(count)
}
