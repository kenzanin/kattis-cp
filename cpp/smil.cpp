#include "bits/stdc++.h"

int main() {
  std::string in1;
  std::cin >> in1;
  for (int i = 0; i < in1.size(); i++) {
    auto c = in1[i];
    if (c == ':' || c == ';') {
      c = in1[i + 1];
      if (c == ')') {
        std::cout << i << "\n";
        i++;
        continue;
      } else if (c == '-' && (in1[i + 2] == ')')) {
        std::cout << i << "\n";
        i++;
        i++;
        continue;
      }
    }
  }
}