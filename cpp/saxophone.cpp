#include <algorithm>
#include <array>
#include <bitset>
#include <iostream>
#include <map>

const std::map<char, std::bitset<10>> saxMap{
    {'c', std::bitset<10>("0111001111")}, {'d', std::bitset<10>("0111001110")},
    {'e', std::bitset<10>("0111001100")}, {'f', std::bitset<10>("0111001000")},
    {'g', std::bitset<10>("0111000000")}, {'a', std::bitset<10>("0110000000")},
    {'b', std::bitset<10>("0100000000")}, {'C', std::bitset<10>("0010000000")},
    {'D', std::bitset<10>("1111001110")}, {'E', std::bitset<10>("1111001100")},
    {'F', std::bitset<10>("1111001000")}, {'G', std::bitset<10>("1111000000")},
    {'A', std::bitset<10>("1110000000")}, {'B', std::bitset<10>("1100000000")},
};

int main() {
  int a;
  std::cin >> a;
  std::cin.ignore();

  for (int i = 0; i < a; i++) {
    std::string b{};
    std::getline(std::cin, b);
    if (b.size() == 0) {
      std::cout << "0 0 0 0 0 0 0 0 0 0\n";
      continue;
    }
    std::array<int, 10> press{};
    const auto tmp = saxMap.at(b[0]);
    for (int i = 0; i < 10; i++) {
      if (tmp.test(i)) {
        press[i] = 1;
      }
    }

    for (int i = 0; i < b.size() - 1; i++) {
      const auto x = saxMap.at(b[i]);
      const auto y = saxMap.at(b[i + 1]);
      const auto z = (~x) & y;
      for (int ii = 0; ii < 10; ii++) {
        if (z.test(ii)) {
          press[ii]++;
        }
      }
    }
    std::for_each(
        press.rbegin(), press.rend(),
        [](const auto &e){ std::cout << e << " "; });
    std::cout << "\n";
  }
}
