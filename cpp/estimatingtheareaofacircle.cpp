#include "bits/stdc++.h"

int main() {
  double a, b, c;
  while (true) {
    std::cin >> a >> b >> c;
    if (a == 0 && b == 0 && c == 0) {
      break;
    }
    std::cout << M_PI * std::pow(a, 2) << " ";
    std::cout << (a + a) * (a + a) / (b / c) << "\n";
  }
}