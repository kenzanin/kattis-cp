#include "bits/stdc++.h"

int main() {
  int a;
  std::cin >> a;
  std::string b{}, c{};
  std::cin >> b >> c;
  int r{}, s{};
  std::unordered_map<char, int> count_b, count_c;

  for (int i = 0; i < b.length(); i++) {
    if (b[i] == c[i]) {
      r++;
    } else {
      count_b[b[i]]++;
      count_c[c[i]]++;
    }
  }

  for (auto &pair : count_b) {
    s += std::min(pair.second, count_c[pair.first]);
  }

  std::cout << r << " " << s << "\n";
}
