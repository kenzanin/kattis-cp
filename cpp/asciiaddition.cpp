#include "bits/stdc++.h"

namespace {
typedef std::array<std::string, 7> set_angka;

class ascii_num {
public:
  void const getInput();
  void const puts();

private:
  const set_angka _zero{"xxxxx", "x...x", "x...x", "x...x",
                        "x...x", "x...x", "xxxxx"};
  const set_angka _one{"....x", "....x", "....x", "....x",
                       "....x", "....x", "....x"};
  const set_angka _two{"xxxxx", "....x", "....x", "xxxxx",
                       "x....", "x....", "xxxxx"};
  const set_angka _three{"xxxxx", "....x", "....x", "xxxxx",
                         "....x", "....x", "xxxxx"};
  const set_angka _four{"x...x", "x...x", "x...x", "xxxxx",
                        "....x", "....x", "....x"};
  const set_angka _five{"xxxxx", "x....", "x....", "xxxxx",
                        "....x", "....x", "xxxxx"};
  const set_angka _six{"xxxxx", "x....", "x....", "xxxxx",
                       "x...x", "x...x", "xxxxx"};
  const set_angka _seven{"xxxxx", "....x", "....x", "....x",
                         "....x", "....x", "....x"};
  const set_angka _eight{"xxxxx", "x...x", "x...x", "xxxxx",
                         "x...x", "x...x", "xxxxx"};
  const set_angka _nine{"xxxxx", "x...x", "x...x", "xxxxx",
                        "....x", "....x", "xxxxx"};
  const set_angka _plus{".....", "..x..", "..x..", "xxxxx",
                        "..x..", "..x..", "....."};

  const std::array<const set_angka *, 11> _arr_ascii{
      &_zero, &_one,   &_two,   &_three, &_four, &_five,
      &_six,  &_seven, &_eight, &_nine,  &_plus};

  const bool compare(const set_angka &a, const set_angka &b);
  const uint64_t calculate();
  const set_angka put(char i);
  std::array<std::string, 2> angka{};
  uint8_t plus = 0;
};

const bool ascii_num::compare(const set_angka &a, const set_angka &b) {
  for (int i = 0; i < 5; i++) {
    if (a[i] != b[i]) {
      return false;
    }
  }
  return true;
}

const void ascii_num::puts() {
  std::string var01(std::to_string(calculate()));

  set_angka hasil{};
  for (auto &e : var01) {
    for (int i = 0; i < hasil.size(); i++) {
      hasil[i] += put(e)[i] + '.';
    }
  }

  for (auto &e : hasil) {
    e.erase(e.end() - 1);
  }

  for (auto &e : hasil) {
    std::cout << e << "\n";
  }
}

const uint64_t ascii_num::calculate() {
  uint64_t a = std::atoi(angka[0].c_str());
  uint64_t b = std::atoi(angka[1].c_str());
  return a + b;
}

const void ascii_num::getInput() {
  std::array<std::list<char>, 7> tmp{};
  for (int i = 0; i < tmp.size(); i++) {
    std::string in{};
    std::getline(std::cin, in);
    tmp[i] = std::list<char>(in.begin(), in.end());
  }

  while (!tmp[0].empty()) {
    set_angka var01{};
    for (int i = 0; i < 7; i++) {
      for (int ii = 0; ii < 5; ii++) {
        var01[i] += tmp[i].front();
        tmp[i].pop_front();
      }
      if (tmp[i].front() == '.') {
        tmp[i].pop_front();
      }
    }
    for (int i = 0; i < _arr_ascii.size(); i++) {
      if (compare(*_arr_ascii[i], var01)) {
        if (i == 10) {
          plus += 1;
          break;
        }
        angka[plus] += i + '0';
        break;
      }
    }
  }
}

const set_angka ascii_num::put(char i) { return *_arr_ascii[i - '0']; }
} // namespace

int main() {
  auto x = std::unique_ptr<ascii_num>(new ascii_num);
  if (!x)
    throw std::runtime_error("error");
  x->getInput();
  x->puts();
  return 0;
}
