#include "bits/stdc++.h"

int main() {
  int in1, in2;
  std::cin >> in1 >> in2;
  std::cout << (in2 < in1 ? 1 : 0);
}