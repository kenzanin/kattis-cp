package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	a := strings.Split(input.Text(), "|")
	input.Scan()
	b := strings.Split(input.Text(), "|")
	fmt.Print(a[0], b[0], " ", a[1], b[1])
}
