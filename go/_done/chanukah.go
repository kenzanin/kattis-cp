package main

import "fmt"

func main() {
	a := 0
	fmt.Scanln(&a)
	for i := 1; i <= a; i++ {
		var b, c int
		fmt.Scan(&b, &c)
		res := ((c * (c + 1)) / 2) + c
		fmt.Println(i, res)
	}
}
