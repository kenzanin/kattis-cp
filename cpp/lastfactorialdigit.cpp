#include "bits/stdc++.h"

int main() {
  int n;
  std::cin >> n;
  for (int i = 0; i < n; i++) {
    int nn;
    std::cin >> nn;
    uint64_t var01 = 1;
    for (int ii = 2; ii < nn + 1; ii++) {
      var01 *= ii;
    }
    std::cout << var01 % 10 << "\n";
  }
  return 0;
}