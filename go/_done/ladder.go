package main

import (
	"fmt"
	"math"
)

func main() {
	a, b := 0, 0
	fmt.Scan(&a, &b)
	rad := (float64(b) * math.Pi) / 180.0
	h := float64(a) / math.Sin(rad)
	fmt.Println(math.Ceil(h))
}
