#include "bits/stdc++.h"

int main() {
  auto detect = [](int e) {
    switch (e) {
    case 'A':
    case 'I':
    case 'U':
    case 'E':
    case 'O': {
      return 1;
    } break;
    default:
      return 0;
    }
  };

  std::string in1(80, 0);
  auto count = std::cin.getline(in1.data(), in1.size()).gcount();
  in1.resize(count);
  int res{};
  for (auto &e : in1) {
    res += detect(std::toupper(e));
  }
  std::cout << res << "\n";
  return 0;
}