#include "bits/stdc++.h"

int main() {
  int in1;
  std::cin >> in1;
  uint64_t res{};
  std::cin >> res;
  for (int i = 1; i < in1; i++) {
    int in2;
    std::cin >> in2;
    res += in2;
    res--;
  }
  std::cout << res << "\n";
}