#include "bits/stdc++.h"

int main() {
  const std::array<int, 64> fmap{0,   2,   20,  22,  200, 202, 220, 222, //
                                 1,   3,   21,  23,  201, 203, 221, 223, //
                                 10,  12,  30,  32,  210, 212, 230, 232, //
                                 11,  13,  31,  33,  211, 213, 231, 233, //
                                 100, 102, 120, 122, 300, 302, 320, 322, //
                                 101, 103, 121, 123, 301, 303, 321, 323, //
                                 110, 112, 130, 132, 310, 312, 320, 332, //
                                 111, 113, 131, 133, 311, 313, 331, 333};

  std::string in1{};
  std::cin >> in1;
  int res[3]{};
  res[0] = in1.length();
  auto cari = std::atoi(in1.c_str());
  int qmap =
      std::distance(fmap.begin(), std::find(fmap.begin(), fmap.end(), cari));
  for (int i = 2; qmap > 0; i--) {
    res[i] = qmap % 8;
    qmap /= 8;
  }
  std::cout << res[0] << " " << res[1] << " " << res[2] << "\n";
}
