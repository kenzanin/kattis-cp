package main

import "fmt"

func main() {
	a := 0
	fmt.Scan(&a)
	ii := 1
	count := 0
	for i := 1; ii < a; i++ {
		ii = i * (i + 1) * (i + 2)
		if ii < a {
			count++
		}
	}
	fmt.Println(count)
}
