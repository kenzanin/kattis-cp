#include "bits/stdc++.h"

int main() {
  int n;
  std::cin >> n;
  for (int i = 0; i < n; i++) {
    std::cout << i + 1 << " Abracadabra\n";
  }
  return 0;
}